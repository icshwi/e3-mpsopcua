require opcua,0.5.2
require iocStats,3.1.16

epicsEnvSet(TOP, "$(E3_CMD_TOP)/..")
epicsEnvSet("IOCNAME", "MPSoS:Ctrl-IOC-01:")
epicsEnvSet("EPICS_DB_INCLUDE_PATH", "$(EPICS_DB_INCLUDE_PATH):$(TOP)/template")

#MPSMag
epicsEnvSet("SESSION-MPSMag", "$(IOCNAME)Mag")
epicsEnvSet("SUBSCRIPT-MPSMag", "S7PLC-MPSMag")
epicsEnvSet("OPCSERVER-MPSMag", "172.16.45.130")
epicsEnvSet("PORT-MPSMag", "4840")
iocshLoad("$(opcua_DIR)/opcua.iocsh", "P=$(SESSION-MPSMag):,SESS=$(SESSION-MPSMag),SUBS=$(SUBSCRIPT-MPSMag),INET=$(OPCSERVER-MPSMag),PORT=$(PORT-MPSMag),DEBUG=1, SUBSDEBUG=0")
dbLoadTemplate("$(TOP)/template/test_opcua.substitutions", "PREFIX=MPSMag:,SUBSCRIPT=$(SUBSCRIPT-MPSMag),DDI=:,DDI2=Ctrl-FCPU-001:,REC=Ctrl-IOC-001:,IOCNAME=$(IOCNAME)")

#Include IOC stats for monitoring of our IOC
iocshLoad("$(iocStats_DIR)/iocStats.iocsh", "IOCNAME=$(IOCNAME)")

iocInit()

dbl > "$(TOP)/$(IOCNAME)_PVs.list"

