require opcua,0.5.2
require iocStats,3.1.16

epicsEnvSet(TOP, "$(E3_CMD_TOP)/..")
epicsEnvSet("IOCNAME", "OPCUA-MPS")
epicsEnvSet("EPICS_DB_INCLUDE_PATH", "$(EPICS_DB_INCLUDE_PATH):$(TOP)/template")

#MPSVac
epicsEnvSet("SESSION-MPSVac",   "$(IOCNAME)VAC")
epicsEnvSet("SUBSCRIPT-MPSVac", "S7PLC-MPSVac")
epicsEnvSet("OPCSERVER-MPSVac", "172.16.45.135")
epicsEnvSet("PORT-MPSVac", "4840")
iocshLoad("$(opcua_DIR)/opcua.iocsh", "P=$(SESSION-MPSVac):,SESS=$(SESSION-MPSVac),SUBS=$(SUBSCRIPT-MPSVac),INET=$(OPCSERVER-MPSVac),PORT=$(PORT-MPSVac),DEBUG=1, SUBSDEBUG=0")
dbLoadTemplate("$(TOP)/template/mpsvac-plc-ess.substitutions", "PREFIX=MPSoS-MPSVac:,SUBSCRIPT=$(SUBSCRIPT-MPSVac),REC=Ctrl-IOC-001:,CTRL=Ctrl-PLC-001:,IOCNAME=$(IOCNAME)")

#MPSID
epicsEnvSet("SESSION-MPSID", "$(IOCNAME)ID")
epicsEnvSet("SUBSCRIPT-MPSID", "S7PLC-MPSID")
epicsEnvSet("OPCSERVER-MPSID", "172.16.45.140")
epicsEnvSet("PORT-MPSID", "4840")
iocshLoad("$(opcua_DIR)/opcua.iocsh", "P=$(SESSION-MPSID):,SESS=$(SESSION-MPSID),SUBS=$(SUBSCRIPT-MPSID),INET=$(OPCSERVER-MPSID),PORT=$(PORT-MPSID),DEBUG=1, SUBSDEBUG=0")
dbLoadTemplate("$(TOP)/template/mpsid-plc-ess.substitutions", "PREFIX=MPSoS-MPSID:,SUBSCRIPT=$(SUBSCRIPT-MPSID),REC=Ctrl-IOC-001:,CTRL=Ctrl-PLC-002:,IOCNAME=$(IOCNAME)")

#MPSMag
epicsEnvSet("SESSION-MPSMag", "$(IOCNAME)Mag")
epicsEnvSet("SUBSCRIPT-MPSMag", "S7PLC-MPSMag")
epicsEnvSet("OPCSERVER-MPSMag", "172.16.45.130")
epicsEnvSet("PORT-MPSMag", "4840")
iocshLoad("$(opcua_DIR)/opcua.iocsh", "P=$(SESSION-MPSMag):,SESS=$(SESSION-MPSMag),SUBS=$(SUBSCRIPT-MPSMag),INET=$(OPCSERVER-MPSMag),PORT=$(PORT-MPSMag),DEBUG=1, SUBSDEBUG=0")
dbLoadTemplate("$(TOP)/template/mpsmag-plc-ess.substitutions", "PREFIX=MPSoS-MPSMag:,SUBSCRIPT=$(SUBSCRIPT-MPSMag),REC=Ctrl-IOC-001:,CTRL=Ctrl-PLC-003:,IOCNAME=$(IOCNAME)")

#Include IOC stats for monitoring of our IOC
iocshLoad("$(iocStats_DIR)/iocStats.iocsh", "IOCNAME=$(IOCNAME)")

iocInit()

dbl > "$(TOP)/$(IOCNAME)_PVs.list"

