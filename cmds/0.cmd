require opcua,0.5.2
require iocStats,3.1.16

epicsEnvSet(TOP, "$(E3_CMD_TOP)/..")

epicsEnvSet("IOCNAME",    "OPC-MPSVAC")

epicsEnvSet("SESSION",   "OPC_VVS_00")
epicsEnvSet("SUBSCRIPT", "S7PLC")
epicsEnvSet("OPCSERVER", "172.16.45.135")

iocshLoad("$(iocStats_DIR)/iocStats.iocsh", "IOCNAME=$(IOCNAME)")
iocshLoad("$(opcua_DIR)/opcua.iocsh", "P=OPC-MPSVAC:,SESS=$(SESSION),SUBS=$(SUBSCRIPT),INET=$(OPCSERVER),PORT=4840,DEBUG=1, SUBSDEBUG=0")



#dbLoadRecords("$(TOP)/template/MPSVac_VVS.db", "P=OPC-MPSVAC:,R=S7PLC-MPSVac:,SUBS=$(SUBSCRIPT)")
#dbLoadRecords("$(TOP)/template/MPSVac_State_Machine.db", "P=OPC-MPSVac:,R=S7PLC-MPSVac:,SUBS=$(SUBSCRIPT)")
#dbLoadRecords("$(TOP)/template/MPSVac_Masking.db", "P=OPC-MPSVac:,R=S7PLC-MPSVac:,SUBS=$(SUBSCRIPT)")
#dbLoadRecords("$(TOP)/template/MPSVac_BM_BD.db", "P=OPC-MPSVac:,R=S7PLC-MPSVac:,SUBS=$(SUBSCRIPT)")
#dbLoadRecords("$(TOP)/template/MPSVac_FBIS.db", "P=OPC-MPSVac:,R=S7PLC-MPSVac:,SUBS=$(SUBSCRIPT)")
#dbLoadRecords("$(TOP)/template/MPSVac_Rearm_Reint.db", "P=OPC-MPSVac:,R=S7PLC-MPSVac:,SUBS=$(SUBSCRIPT)")
#dbLoadRecords("$(TOP)/template/MPSVac_Checksum.db", "P=OPC-MPSVac:,R=S7PLC-MPSVac:,SUBS=$(SUBSCRIPT)")
#dbLoadRecords("$(TOP)/template/MPSVac_Diagnostics.db", "P=OPC-MPSVac:,R=S7PLC-MPSVac:,SUBS=$(SUBSCRIPT)")
#dbLoadRecords("$(TOP)/template/MPSVac_WatchDogs.db", "P=OPC-MPSVac:,R=S7PLC-MPSVac:,SUBS=$(SUBSCRIPT)")


epicsEnvSet("EPICS_DB_INCLUDE_PATH", "$(EPICS_DB_INCLUDE_PATH):$(TOP)/template")
dbLoadTemplate("$(TOP)/template/mpsvac-plc-ess.substitutions", "PREFIX=MPSoS-MPSVac:,SUBSCRIPT=$(SUBSCRIPT),REC=Ctrl-IOC-001:,IOCNAME=$(IOCNAME)")

#dbLoadRecords("$(TOP)/template/mpsvac-plc-ess.db", "PREFIX=MPSoS-MPSVac:,SUBSCRIPT=$(SUBSCRIPT),REC=Ctrl-IOC-001:")

iocInit()

dbl > "$(TOP)/$(SESSION)_PVs.list"

