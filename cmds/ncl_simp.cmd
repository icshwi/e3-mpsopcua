require opcua,0.5.2
require iocStats,3.1.16

epicsEnvSet(TOP, "$(E3_CMD_TOP)/..")
epicsEnvSet("IOCNAME", "MPSoS:Ctrl-IOC-01:")
epicsEnvSet("EPICS_DB_INCLUDE_PATH", "$(EPICS_DB_INCLUDE_PATH):$(TOP)/template")

#MPSVac
epicsEnvSet("SESSION-MPSVac",   "$(IOCNAME)VAC")
epicsEnvSet("SUBSCRIPT-MPSVac", "S7PLC-MPSVac")
epicsEnvSet("OPCSERVER-MPSVac", "172.16.45.135")
epicsEnvSet("PORT-MPSVac", "4840")
iocshLoad("$(opcua_DIR)/opcua.iocsh", "P=$(SESSION-MPSVac):,SESS=$(SESSION-MPSVac),SUBS=$(SUBSCRIPT-MPSVac),INET=$(OPCSERVER-MPSVac),PORT=$(PORT-MPSVac),DEBUG=1, SUBSDEBUG=0")
dbLoadTemplate("$(TOP)/template/mpsvac-plc-ess_ncl_simp.substitutions", "PREFIX=MPSVac:,SUBSCRIPT=$(SUBSCRIPT-MPSVac),DDI=:,DDI2=Ctrl-FCPU-001:,REC=Ctrl-IOC-001:,IOCNAME=$(IOCNAME)")

#MPSID
epicsEnvSet("SESSION-MPSID", "$(IOCNAME)ID")
epicsEnvSet("SUBSCRIPT-MPSID", "S7PLC-MPSID")
epicsEnvSet("OPCSERVER-MPSID", "172.16.45.140")
epicsEnvSet("PORT-MPSID", "4840")
iocshLoad("$(opcua_DIR)/opcua.iocsh", "P=$(SESSION-MPSID):,SESS=$(SESSION-MPSID),SUBS=$(SUBSCRIPT-MPSID),INET=$(OPCSERVER-MPSID),PORT=$(PORT-MPSID),DEBUG=1, SUBSDEBUG=0")
dbLoadTemplate("$(TOP)/template/mpsid-plc-ess_ncl_simp.substitutions", "PREFIX=MPSID:,SUBSCRIPT=$(SUBSCRIPT-MPSID),DDI=:,DDI2=Ctrl-FCPU-001:,REC=Ctrl-IOC-001:,IOCNAME=$(IOCNAME)")
#dbLoadTemplate("$(TOP)/template/mpsid-plc-ess_ncl_nobuf.substitutions", "PREFIX=MPSID:,SUBSCRIPT=$(SUBSCRIPT-MPSID),DDI=:,DDI2=Ctrl-FCPU-001:,REC=Ctrl-IOC-001:,IOCNAME=$(IOCNAME)")

#MPSMag
epicsEnvSet("SESSION-MPSMag", "$(IOCNAME)Mag")
epicsEnvSet("SUBSCRIPT-MPSMag", "S7PLC-MPSMag")
epicsEnvSet("OPCSERVER-MPSMag", "172.16.45.130")
epicsEnvSet("PORT-MPSMag", "4840")
iocshLoad("$(opcua_DIR)/opcua.iocsh", "P=$(SESSION-MPSMag):,SESS=$(SESSION-MPSMag),SUBS=$(SUBSCRIPT-MPSMag),INET=$(OPCSERVER-MPSMag),PORT=$(PORT-MPSMag),DEBUG=1, SUBSDEBUG=0")
dbLoadTemplate("$(TOP)/template/mpsmag-plc-ess_ncl_simp.substitutions", "PREFIX=MPSMag:,SUBSCRIPT=$(SUBSCRIPT-MPSMag),DDI=:,DDI2=Ctrl-FCPU-001:,REC=Ctrl-IOC-001:,IOCNAME=$(IOCNAME)")

#Include IOC stats for monitoring of our IOC
iocshLoad("$(iocStats_DIR)/iocStats.iocsh", "IOCNAME=$(IOCNAME)")

iocInit()

dbl > "$(TOP)/$(IOCNAME)_PVs.list"

